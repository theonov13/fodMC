# Main developer
Core routines, code structure and working examples.

#### Kai Trepte, PhD  
email: kai.trepte1987@gmail.com
- all main routines

#### Sebastian Schwalbe, PhD
email: theonov13@gmail.com
- pyfodMC_GUI 
- source code structuring 
- ideas for improvements of the fodMC (simplification of input) 

# Former co-developers:
Additional features, code testing and improvements.

#### Alex Johnson
email: johns1ai@cmich.edu
- initial idea for the connectivity matrix 

#### Jakob Kraus
email: jakob.kraus@student.tu-freiberg.de
- improvement for the connectivity matrix 
